type Point = [number, number]
type Movement = 'U' | 'D' | 'L' | 'R'

function calculPosition([absLimit, ordLimit]: Point, [abs, ord]: Point, movement: Movement): Point {
    switch(movement) {
        case 'U': return [abs, ord - 1 >= 0 ? ord - 1 : 0]
        case 'D': return [abs, ord + 1 <= ordLimit ? ord + 1 : ordLimit]
        case 'L': return [abs - 1 >= 0 ? abs - 1 : 0, ord]
        case 'R': return [abs + 1 <= absLimit ? abs + 1 : absLimit, ord]
    }
}

function calculPortal(portalA: Point, portalB: Point, position: Point) {
    if (position[0] == portalA[0] && position[1] == portalA[1]) return portalB
    if (position[0] == portalB[0] && position[1] == portalB[1]) return portalA
    return position
}

function hasPortals(portals: [Point, Point] | []): portals is [Point, Point] {
    return portals.length > 0
}

export function moveCharacter(gridLimit: Point, portals: [Point, Point] | [], initialPosition: Point, movements: Movement[]): Point {
    return movements.reduce<Point>((position, movement) => {
        const positionTmp = calculPosition(gridLimit, position, movement)
        return hasPortals(portals) ? calculPortal(...portals, positionTmp) : positionTmp
    }, initialPosition)
}
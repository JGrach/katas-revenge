## Exercise explanation

We have an array of F1 teams
We have an array of races containing an array of points
    Array of points is sorted to corresponds of F1 teams for example.

example: 
```
const result = meanPointByRace(
    ['RedBull', 'Mercedes', ...],
    [
        [25, 12, ...], 
        [12, 25, ...], 
    ]
)
```

In this above example, at the first race Redbull won 25 points and Mercedes 12, at the second race, Redbull won 12 points and Mercedes 25.

The function have to output the three firsts team of a list of race, with the mean of points won by race on the following format:
```
const result = [
    {team: /*name of the first team*/, pointByRace: /*points won by races*/},
    {team: /*name of the second team*/, pointByRace: /*points won by races*/},
    {team: /*name of the third team*/, pointByRace: /*points won by races*/},
]
```
## Exercise explanation

We have an ATM.
We enter an array of available bills value.
We enter an amount.

ATM should send the amount with the smallest number of bills.

## Intuitive approach

In a recursive loop, divide amount by the biggest bill value then divide modulo by the second biggest bill value until amount is reach.

## Avoid trap approach

Intuitive approach is not enough. Modulo of the biggest bill could result in more bills than a lower division. For instance `const bills = [1, 20, 50]` should result in 3 bill of 20 and not 1 bill of 50 and 10 bill of 1.

My second approach will check number of bills given with `[1]` then with `[1, 20]` and with `[1, 20, 50]`. I suppose problem should be detected when try for each value treat like a maximal.

In a real world I probably ask a data engineer or a data scientist to be sure it cannot be better. I'm not sure of efficiency.




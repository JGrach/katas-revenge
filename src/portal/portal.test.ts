import { moveCharacter } from './portal';
import { expect } from 'chai';

describe('characterMove: should give the position after movement and portal teleport', () => {
    type Point = [abs: number, ord: number]
    type Portals = [Point, Point] | []
    type Movement = 'U' | 'D' | 'L' | 'R'

    type ExpectedFunction = (gridLimit: Point, portals: Portals, initialPosition: Point, movements: Movement[]) => Point


    it("should move as attempted", () => {
        const gridLimit: Point = [6, 6]
        const initialPosition: Point = [2, 2]

        const expectedResult = [3, 5]

        const moveCharacterResult = (moveCharacter as ExpectedFunction)(
            gridLimit, 
            [], 
            initialPosition, 
            ['U', 'L', 'D', 'D', 'D', 'R', 'R', 'D']
        )

        expect(moveCharacterResult).eql(expectedResult)
    })

    it("should move as attempted and not quit the grid", () => {
        const gridLimit: Point = [4, 4]
        const initialPosition: Point = [2, 2]

        const expectedResult = [4, 1]

        const moveCharacterResult = (moveCharacter as ExpectedFunction)(
            gridLimit, 
            [], 
            initialPosition,
            ['U', 'U', 'U', 'R', 'R', 'D', 'R']
        )

        expect(moveCharacterResult).eql(expectedResult)
    })

    it("should move as attempted with portals", () => {
        const gridLimit: Point = [6, 6]
        const initialPosition: Point = [2, 2]
        const portalA: Point = [1,2]
        const portalB: Point = [3,5]

        const expectedResult = [4,5]

        const moveCharacterResult = (moveCharacter as ExpectedFunction)(
            gridLimit, 
            [portalA, portalB], 
            initialPosition, 
            ['D', 'L', 'U', 'R']
        )

        expect(moveCharacterResult).eql(expectedResult)
    })

    it("should move as attempted with portals without quit the grid", () => {
        const gridLimit: Point = [6, 6]
        const initialPosition: Point = [2, 2]
        const portalA: Point = [1,2]
        const portalB: Point = [3,5]

        const expectedResult = [1,3]

        const moveCharacterResult = (moveCharacter as ExpectedFunction)(
            gridLimit, 
            [portalA, portalB], 
            initialPosition, 
            ['U', 'U', 'L', 'U', 'L', 'D', 'D', 'R', 'D', 'D', 'L', 'U', 'R', 'D']
        )

        expect(moveCharacterResult).eql(expectedResult)
    })
}) 
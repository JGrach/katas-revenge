type BillsByValue = [billValue: number, numberOfBills: number]

function giveBills(billsFromHigherToLower: number[], amount: number): BillsByValue[] {
    if (amount === 0) return []
    if (billsFromHigherToLower.length === 0) throw new Error("BILLS_UNAVAILABLE")

    const [bill, ...restBills] = billsFromHigherToLower
    return [[bill, Math.floor(amount / bill)] as BillsByValue].concat(giveBills(restBills, amount % bill))
}

function giveBillsCandidates(availableBills: number[], amount: number) {
    if (availableBills.length === 0) throw new Error("BILLS_UNAVAILABLE")
        
    return availableBills.map((_, billIndex, bills) => {
        // check without bigger values to avoid error (see atm.test.ts)
        const lowerAvailableBills = bills.slice(0, billIndex + 1)
        const lowerAvailableBillsFromHighestToLowest = lowerAvailableBills.reverse()

        return giveBills(lowerAvailableBillsFromHighestToLowest, amount)
    })
}

function countBills(billsByValue: BillsByValue[]): number {
    return billsByValue.reduce((acc, [_, numberOfBills]) => acc + numberOfBills, 0)
}

function formatOutput(billsByValue: BillsByValue[]) {
    return billsByValue.filter(([_, numberOfBill]) => numberOfBill > 0).reverse()
}

export function atm(availableBills: number[], amount: number): BillsByValue[] {
    const possibilities = giveBillsCandidates(availableBills, amount)
    const betterPossibility = possibilities.sort((a, b) => countBills(a) - countBills(b))[0]

    return formatOutput(betterPossibility)
}
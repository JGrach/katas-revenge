export function meanPointByRace(teams: string[], points: number[][]): {team: string, pointByRace: number}[] {
    const totalPoints = points.reduce<number[]>((acc, race) => race.map((point, index) => point + (acc[index] ?? 0)), [])
    const meanPoints = totalPoints.map((point, index) => ({team: teams[index], pointByRace: point / points.length}))
    return meanPoints.sort((a, b) => b.pointByRace - a.pointByRace).slice(0, 3)
}
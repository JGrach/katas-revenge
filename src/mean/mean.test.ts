import { meanPointByRace } from './mean';
import { expect } from 'chai';

describe('meanPointByRace: expected function have to give the three firsts teams sorted by the mean of points won by race ', () => { 
    type Team = 'RedBull' | 'Ferrari' | 'Mercedes' | 'McLaren' | 'AstonMartin' | 'Williams' | 'Sauber' | 'RacingBull' | 'Haas' | 'Alpine' 
    type Point = number
    type Race = Point[]
    type MeanPointByRace = {team: Team, pointByRace: Point}

    type ExpectedFunction = (a: Team[], b: Race[]) => MeanPointByRace[]

    it('should work for 2024 season', () => {
        const teams: Team[] = ['Alpine', 'AstonMartin', 'Ferrari', 'Haas', 'McLaren', 'Mercedes', 'RacingBull', 'RedBull', 'Sauber', 'Williams']
        const races: Race[] = [
            [0, 3, 27, 0, 12, 16, 0, 44, 0, 0],
            [0, 10, 22, 1, 16, 10, 0, 43, 0, 0],
            [0, 12, 44, 3, 27, 0, 6, 10, 0, 0],
            [0, 8, 27, 0, 14, 8, 1, 44, 0, 0]
        ]

        const expectedResult: MeanPointByRace[] = [
            { team: 'RedBull', pointByRace: 35.25 },
            { team: 'Ferrari', pointByRace: 30 },
            { team: 'McLaren', pointByRace: 17.25 } 
        ]

        const meanPointByRaceResult = (meanPointByRace as ExpectedFunction)(teams, races) 

        expect(meanPointByRaceResult).eql(expectedResult)
    });

    it('should work for 2025 season', () => {
        const teams: Team[] = ['Alpine', 'AstonMartin', 'Ferrari', 'Haas', 'McLaren', 'Mercedes', 'RacingBull', 'RedBull', 'Sauber', 'Williams']
        const races: Race[] = [[2,23,12,0,0,16,0,43,4,1],[6,15,14,1,0,22,0,44,0,0],[0,27,0,6,12,18,1,36,2,0],[0,22,36,0,2,20,1,57,0,0],[6,15,16,1,0,20,0,44,0,0],[21,18,12,0,3,23,0,25,0,0],[5,14,10,0,0,33,0,38,2,0],[4,20,22,0,0,15,0,34,1,6],[3,21,32,3,12,11,0,56,0,0],[0,6,3,0,30,25,0,34,0,4],[0,3,10,0,28,20,0,41,0,0],[10,12,24,0,16,24,1,51,0,0],[16,19,10,0,8,8,0,37,0,4],[0,2,27,0,4,18,0,43,1,6],[8,0,37,1,24,16,2,14,0,0],[3,4,20,0,33,16,0,26,0,0],[6,9,13,0,47,21,0,34,6,2],[10,6,24,0,23,18,5,49,0,3],[1,0,27,0,14,27,6,25,0,2],[7,25,13,0,26,11,5,51,0,0],[12,12,26,0,2,10,0,40,0,0],[0,7,18,0,18,17,4,38,0,0]]
        
        const expectedResult: MeanPointByRace[] = [
            { team: 'RedBull', pointByRace: 860/22 },
            { team: 'Mercedes', pointByRace: 409/22 },
            { team: 'Ferrari', pointByRace: 406/22 } 
        ]

        const meanPointByRaceResult = (meanPointByRace as ExpectedFunction)(teams, races) 

        expect(meanPointByRaceResult).eql(expectedResult)
    });
});
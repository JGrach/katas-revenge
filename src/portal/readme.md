## Exercise explanation

We have a grid from [0, 0] into a grid limit defined by a grid limit point [x, y] (x abs, y ord).
We have a character placed on an initial value [x, y].
We have a list of movement the character have to traverse (an array of letter: U for up D for down L for left and R for Right)
We have 2 portals at 2 distincts points [x, y] of the grid.

example:
```
const result = moveCharacter(
    [5, 5], // grid limit 
    [[1, 2], [4, 4]], // 2 portals
    [2, 3], // initial position
    ['U', 'L', 'D', 'D', 'D', 'R', 'R', 'D'] // list of movement
)
```

Function have to output the final position with this rules:
    - a movement is forbidden if the character have to get out from the grid, a forbidden movement is canceled
    - if the character walk on a portal, it's immediately teleported at the other portal

final position is a point:

```
const result = [/* coordinates */]
```
import { fizzbuzz } from './fizzbuzz';
import { expect } from 'chai';

describe("fizzbuzz: expected function have to count with fizz buzz", () => {
    it("should count until the input number", () => {
        const max = 15
        const expected = fizzbuzz(max)
        expect(expected).lengthOf(max)
        expect(expected[0]).eql('1')
        expect(expected[1]).eql('2')
        expect(expected[3]).eql('4')
        expect(expected[6]).eql('7')
        expect(expected[7]).eql('8')
        expect(expected[10]).eql('11')
        expect(expected[12]).eql('13')
        expect(expected[13]).eql('14')
    })
    it("should replace all number divisible by 3", () => {
        const max = 15
        const expected = fizzbuzz(max)
        expect(expected[2]).eql('fizz')
        expect(expected[5]).eql('fizz')
        expect(expected[8]).eql('fizz')
        expect(expected[11]).eql('fizz')
    })
    it("should replace all number divisible by 5", () => {
        const max = 15
        const expected = fizzbuzz(max)
        expect(expected[4]).eql('buzz')
        expect(expected[9]).eql('buzz')
    })
    it("should replace all number divisible by 3 and by 5", () => {
        const max = 15
        const expected = fizzbuzz(max)
        expect(expected[14]).eql('fizzbuzz')
    })
})

function replaceNumber(num: number): string {
    const fizz = num % 3 == 0 ? 'fizz' : ''
    const buzz = num % 5 == 0 ? 'buzz' : ''
    return (fizz.length > 1 || buzz.length > 1) ? `${fizz}${buzz}` : `${num}`
}

export function fizzbuzz(max: number): string[] {
    if (max <= 0) return []

    return [...fizzbuzz(max - 1), replaceNumber(max)]
}
import { atm } from './atm';
import { expect } from 'chai';

describe("atm: expected function have to give the lowest number of availables bills", () => {
    it("should work on a easy way", () => {
        const bills = [5, 10, 20, 50, 100]
        const result = atm(bills, 195)
        expect(result).eql([[5, 1], [20, 2], [50, 1], [100, 1]])
    })
    it("should work when the higher bill is not efficient", () => {
        const bills = [1, 20, 50]
        const result = atm(bills, 60)
        expect(result).eql([[20, 3]])
    })
    it("should doesn't work when amount couldn't be reach", () => {
        const bills = [100]
        try {
            atm(bills, 50)
        } catch (error) {
            expect(error).instanceOf(Error)
            expect((error as Error).message).eq("BILLS_UNAVAILABLE")
        }
    })
    it("should doesn't work when no bills available", () => {
        const bills: number[] = []
        try {
            atm(bills, 50)
        } catch (error) {
            expect(error).instanceOf(Error)
            expect((error as Error).message).eq("BILLS_UNAVAILABLE")
        }
    })
})